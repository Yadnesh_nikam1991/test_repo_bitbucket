=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager

-----> 5


=====> IBM Installation Manager> Uninstall

There is no package to uninstall.
     C. Cancel
-----> [C] 

=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager

-----> 1
Checking repositories...
Loading repositories...
Checking availability of packages...


=====> IBM Installation Manager> Install

Select packages to install:
     1. [ ] IBM® Rational® ClearCase® - Linux x86 8.0.1.00


     C. Cancel
-----> [C] 1

=====> IBM Installation Manager> Install> Select

IBM® Rational® ClearCase® - Linux x86 8.0.1.00

Options:
     1. Choose version 8.0.1.00 for installation.
     2. Show all available versions of the package.

     C. Cancel
-----> [1] 1
Preparing and resolving the selected packages...
Loading fixes...


=====> IBM Installation Manager> Install

Select packages to install:
     1. [X] IBM® Rational® ClearCase® - Linux x86 8.0.1.00

     O. Check for Other Versions, Fixes, and Extensions

     N. Next,      C. Cancel
-----> [N] 


=====> IBM Installation Manager> Install> Licenses

Read the following license agreements carefully.
View a license agreement by entering the number:
     1. IBM Rational ClearCase - License Agreement

Options:
     A. [ ] I accept the terms in the license agreement
     D. [ ] I do not accept the terms in the license agreement

     B. Back,      C. Cancel
-----> [C] A


=====> IBM Installation Manager> Install> Licenses

Read the following license agreements carefully.
View a license agreement by entering the number:
     1. IBM Rational ClearCase - License Agreement

Options:
     A. [X] I accept the terms in the license agreement
     D. [ ] I do not accept the terms in the license agreement

     B. Back,      N. Next,      C. Cancel
-----> [N] 


=====> IBM Installation Manager> Install> Licenses> Shared Directory

Shared Resources Directory:
        /opt/ibm/IMShared

Options:
     M. Change Shared Resources Directory    

     B. Back,      N. Next,      C. Cancel
-----> [N] 
Finding compatible package groups...


=====> IBM Installation Manager> Install> Licenses> Shared Directory> Location

New package group:
        IBM Rational SDLC

Selected group id: "IBM Rational SDLC" 
Selected location: "/opt/ibm/RationalSDLC"
Selected architecture: 32-bit

Options:
     M. Change Location

     B. Back,      N. Next,      C. Cancel
-----> [N] 


=====> IBM Installation Manager> Install> Licenses> Shared Directory> 
  Location> Translations

Select the translations to install.

Translations Supported by All Packages
     1. [X] English
     2. [ ] Brazilian Portuguese
     3. [ ] French
     4. [ ] German
     5. [ ] Italian
     6. [ ] Japanese
     7. [ ] Korean
     8. [ ] Simplified Chinese
     9. [ ] Spanish
    10. [ ] Traditional Chinese

     B. Back,      N. Next,      C. Cancel
-----> [N] 


=====> IBM Installation Manager> Install> Licenses> Shared Directory> 
  Location> Translations> Features

IBM® Rational® ClearCase® - Linux x86
     1. [ ] ClearCase Full Function Installation
     2. [X] ClearCase Minimal Developer Installation
     3. [ ] ClearCase Server-only Installation
     4. [ ] ClearCase z/OS Extensions
     5. [ ] ClearTeam Explorer Update Site
     6. [ ] Cadence Virtuoso Integration
     7. [ ] Multisite Full Function Installation
     8. [ ] Multisite Shipping Server-only Installation
     9. [ ] ClearQuest Integration with ClearCase
    10. [ ] CCRC WAN Server

     B. Back,      N. Next,      C. Cancel
-----> [N] 1


=====> IBM Installation Manager> Install> Licenses> Shared Directory> 
  Location> Translations> Features

IBM® Rational® ClearCase® - Linux x86
     1. [X] ClearCase Full Function Installation
     2. [X] ClearCase Minimal Developer Installation
     3. [ ] ClearCase Server-only Installation
     4. [ ] ClearCase z/OS Extensions
     5. [ ] ClearTeam Explorer Update Site
     6. [ ] Cadence Virtuoso Integration
     7. [ ] Multisite Full Function Installation
     8. [ ] Multisite Shipping Server-only Installation
     9. [ ] ClearQuest Integration with ClearCase
    10. [ ] CCRC WAN Server

     B. Back,      N. Next,      C. Cancel
-----> [N] 2


=====> IBM Installation Manager> Install> Licenses> Shared Directory> 
  Location> Translations> Features

IBM® Rational® ClearCase® - Linux x86
     1. [X] ClearCase Full Function Installation
     2. [ ] ClearCase Minimal Developer Installation
     3. [ ] ClearCase Server-only Installation
     4. [ ] ClearCase z/OS Extensions
     5. [ ] ClearTeam Explorer Update Site
     6. [ ] Cadence Virtuoso Integration
     7. [ ] Multisite Full Function Installation
     8. [ ] Multisite Shipping Server-only Installation
     9. [ ] ClearQuest Integration with ClearCase
    10. [ ] CCRC WAN Server

     B. Back,      N. Next,      C. Cancel
-----> [N] 

=====> IBM Installation Manager> Install> Licenses> Shared Directory> 
  Location> Translations> Features> Custom panels


---- Common configuration:

Enter a temporary directory. 
 The temporary directory is used to cache temporary files during installation. The files are removed after the installation finishes. 

 Make sure to select a directory that has at least 1.5GB free space.

Temporary Directory: /tmp

   K. Keep Temp directory location
   M. Change Temp directory location

-----> [K] 
Configure IBM Rational Common Licensing 
(powered by FLEXlm software)
 
Enter the TCP/IP port and host name of one or more single license servers, separating the server names with a semicolon. If you are entering redundant license servers, specify three servers separating server names with a comma. If you would like to use the server's default port number, you may omit the port number when specifying a license server. 
 
If you do not know this information, you can configure the license server after the installation completes by running the IBM Rational License Key Administrator. 
 
Example: 
	 @ss1;27000@ss2;27000@rs1,1765@rs2,@rs3;@ss3

   K. Keep License Server
   M. Modify License Server

-----> [K] M

License server elements:
-----> 27000@172.16.88.86

---- Configuration for IBM® Rational® ClearCase® - Linux x86 8.0.1.00


Registry server host name (required):
-----> rilscm4

UNIX registry region (required):
-----> rilccunix

SMTP Mail server host name:
-----> 
Do you want to rebuild the MVFS module during installation?

   Y. Yes, rebuild MVFS using the current kernel.
		The default kernel source directory for your system is : /lib/modules/3.10.0-123.el7.x86_64/build .
   N. No, install pre-built MVFS.

-----> [Y] Y

Enter the path to your top level kernel source directory:

The current kernel source directory is :  .
The default kernel source directory for your system is : /lib/modules/3.10.0-123.el7.x86_64/build .

-----> /lib/modules/3.10.0-123.el7.x86_64/
ClearCase Atria Licensing 
 Configure ClearCase (Atria) Licensing						 
 Selecting Atria licensing overrides common licensing in Rational ClearCase.

   Y. Yes, I want to use Atria licensing.
   N. No, I don't want to use Atria licensing. 

-----> [N] 


     B. Back,      N. Next,      C. Cancel
-----> [N] 


=====> IBM Installation Manager> Install> Licenses> Shared Directory> 
  Location> Translations> Features> Custom panels> Summary

Target Location:
  Package Group Name         :  IBM Rational SDLC
  Installation Directory     :  /opt/ibm/RationalSDLC
  Shared Resources Directory :  /opt/ibm/IMShared

Translations:
        English

Packages to be installed:
        IBM® Rational® ClearCase® - Linux x86 8.0.1.00

Options:
     G. Generate an Installation Response File

     B. Back,      I. Install,      C. Cancel
-----> [I] I
                 25%                50%                75%                100%
------------------|------------------|------------------|------------------|
............................................................................

=====> IBM Installation Manager> Install> Licenses> Shared Directory> 
  Location> Translations> Features> Custom panels> Summary> Completion

The install completed successfully.

Options:
     F. Finish
-----> [F] F

=====> IBM Installation Manager

Select:
     1. Install - Install software packages
     2. Update - Find and install updates and fixes to installed software packages
     3. Modify - Change installed software packages
     4. Roll Back - Revert to an earlier version of installed software packages
     5. Uninstall - Remove installed software packages

Other Options:
     L. View Logs
     S. View Installation History
     V. View Installed Packages
        ------------------------
     P. Preferences
        ------------------------
     A. About IBM Installation Manager
        ------------------------
     X. Exit Installation Manager

-----> X
[root@ril1506g1kb152 tools]# ps -ef | grep albd
root     31258     1  0 15:08 ?        00:00:00 /opt/rational/clearcase/etc/albd_server
root     31371  3412  0 15:08 pts/0    00:00:00 grep --color=auto albd
[root@ril1506g1kb152 tools]# cd /etc/init.d
[root@ril1506g1kb152 init.d]# ls
aksusbd    functions  iprinit    netconsole  README
clearcase  iprdump    iprupdate  network
[root@ril1506g1kb152 init.d]# /usr/atria/bin/cleartool -ver
ClearCase version 8.0.1.00 (Mon May 27 15:08:54 EDT 2013) (8.0.1.D130526)
cleartool                         8.0.1.0 (Wed May 15 14:51:54 2013)
db_server                         8.0.1.0 (Wed May 15 14:50:52 2013)
VOB database schema versions: 54, 80

